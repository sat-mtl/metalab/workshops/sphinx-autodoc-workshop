# Sphinx - autodoc workshop

[Sphinx documentation about autodoc](https://www.sphinx-doc.org/en/master/usage/quickstart.html?highlight=autodoc#autodoc)

## Before we start

A note about side effects in Python and autodoc:

> *Warning* : autodoc imports the modules to be documented. If any modules have side effects on import, these will be executed by autodoc when sphinx-build is run.

> If you document scripts (as opposed to library modules), make sure their main routine is protected by a if __name__ == '__main__' condition.


## First steps

Follow these steps of the first Sphinx workshop:

- get started
- installing Sphinx in a virtualenv
- `sphinx-quickstart` a project

## Adding a test module to the repo

To help us understand how to use the `autodoc` Sphinx extension, we will use a test module.

Copy the folder `module` in your repo. You will find the `module` folder in this repo.

## Configure sphinx to use autodoc

We are now ready to follow the doc on autodoc: [Sphinx documentation about autodoc](https://www.sphinx-doc.org/en/master/usage/quickstart.html?highlight=autodoc#autodoc)

### Make changes to config.py

In the `config.py` file located at `docs/source/config.py`, add the following line:

    extensions = ['sphinx.ext.autodoc']

### Add test_module.rst

Copy the `test_module.rst` file in the `/docs/source` folder to your repo. This contains the necessary instructions to create a HTML file related to the module we added to the repo.

### Add the path to config.py

Add the following line to config.py:

    sys.path.insert(0,os.path.abspath('../..'))

You might need to replace with your actual path to your module instead of the '../..' path.

## Build the project

You are now ready to build the project !

    sphinx-build -b html -D language=en source build/html/en

### Double-check that you understand the side effect of autodoc

If you look at the log, you should see that "this get printed!" was printed - what are ways we could have avoided this?

    
## View the project

Serve locally:

    python3 -m http.server

In a web browser, navigate to `http://0.0.0.0:8000/build/html/en/test_module.html`.



